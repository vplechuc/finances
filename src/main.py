import datetime as dt
import matplotlib.pyplot as plt
from matplotlib import style
import pandas as pd
# fix to ImportError: cannot import name 'is_list_like'
# https://stackoverflow.com/questions/50394873/import-pandas-datareader-gives-importerror-cannot-import-name-is-list-like/
pd.core.common.is_list_like = pd.api.types.is_list_like 
import pandas_datareader.data as web

from mpl_finance import candlestick_ohlc
import matplotlib.dates as mdates

style.use('ggplot')

start = dt.datetime(2015, 1, 1) 
end = dt.datetime.now() 

# uncomment this lines the firt time to get the data from web
# df = web.DataReader("TSLA", 'morningstar', start, end) 
# df.reset_index(inplace=True) 
# df.set_index("Date", inplace=True) 
# df = df.drop("Symbol", axis=1) 
# df.to_csv('TSLA.csv')


df = pd.read_csv('tsla.csv', parse_dates=True, index_col=0)


# finding the average of a 100 lines
df['100ma'] = df['Close'].rolling(window=100, min_periods =0 ).mean()

df_ohlc = df['Close'].resample('10D').ohlc()

df_volume = df['Volume'].resample('10D').sum()


print(df_ohlc.head())

df_ohlc = df_ohlc.reset_index()

df_ohlc['Date'] = df_ohlc['Date'].map(mdates.date2num)

fig = plt.figure()
ax1 = plt.subplot2grid((6,1), (0,0), rowspan=5, colspan=1)
ax2 = plt.subplot2grid((6,1), (5,0), rowspan=1, colspan=1,sharex=ax1)
ax1.xaxis_date()

candlestick_ohlc(ax1, df_ohlc.values, width=2, colorup='g')

ax2.fill_between(df_volume.index.map(mdates.date2num),df_volume.values,0)

# ax1 = plt.subplot2grid((6,1), (0,0), rowspan=5, colspan=1)
# ax2 = plt.subplot2grid((6,1), (5,0), rowspan=1, colspan=1,sharex=ax1)

# ax1.plot(df.index, df['Close'])
# ax1.plot(df.index, df['100ma'])
# ax2.bar(df.index, df['Volume'])

plt.show()